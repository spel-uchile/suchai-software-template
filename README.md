# SUCHAI Flight Software template

Template to create software projects based on the SUCHAI Flight Software

## Quick start

1. Clone this repository in the **framework** branch and initialize SUCHAI Flight Software core repository

```shell
git clone -b framework https://gitlab.com/spel-uchile/suchai-software-template.git
cd suchai-software-template
sh init.sh
```

2. Build the example **simple** application

```shell
cmake -B build -DAPP=simple && cmake --build build
```

3. Execute

```shell
./build/apps/simple/suchai-app
```

## Create new apps

Use the **simple** or **suchai** application as templates to start building
your own app. Simply copy and paste the example application directory and
change the `-DAPP` variable to build your app, ex:

```shell
cp -r app/simple app/myapp
cmake -B build -DAPP=myapp && cmake --build build
./build/apps/myapp/suchai-app
```

Now modify or create source files. Do not forget adding your source files 
to the CMakeList file located in `app/<app_name>/CMakeList.txt` 

## Working with the RaspberryPi (RPI)
Building for the Raspberry Pi is straight forward and very similar to any Linux.
Just connect to the RPi using SSH and clone (or transfer with scp/rsync) this
repository. Then set proper build variables and build with the following steps:

```shell
git clone -b framework https://gitlab.com/spel-uchile/suchai-software-template.git
cd suchai-software-template
sh init.sh
cmake -B build -DAPP=simple -DSCH_ARCH=RPI -DSCH_ST_MODE=SQLITE && cmake --build build
./build/apps/simple/suchai-app
```

## Working with the SUCHAI (NANOMIND)

Building for architectures other than Linux/X86 and Linux/RPI require some
specific workarounds. Each architecture require specific toolchains and 
project organization and libraries. In particular the **suchai** application can be build for the NANOMIND A3200
on board computer (AVR32UC3 with FreeRTOS and GomSapce SDK).

To build the **suchai** application (or derived) do the following:

1. Download or update the SUCHAI FS core repository
```shell
git clone -b framework https://gitlab.com/spel-uchile/suchai-software-template.git
cd suchai-software-template
sh init.sh
cd suchai-flight-software
git pull
cd -
```

2. Install AVR32 toolchain
```shell
cd suchai-flight-software/src/drivers/nanomind
sh install-toolchain.sh
cd -
```

3. Check you correctly installed the AVR32 toolchain
```shell
source ~/.profile
export LC_ALL=C
avr32-gcc --version
```

4. Install Nanomind A3200 SDK. You may require access to some private repositories.
```shell
cd apps/suchai/src/drivers
sh install.sh
cd -
```

5. Configure the project using CMake. Note that we use CMake only to configure
the project (generate config.h files) not to actually build the project. Please ignore
any error similar to: *add_subdirectory given source "freertos/FreeRTOS" which is not an existing directory* (See note 1)
```shell
cmake -B build-suchai -DAPP=suchai -DSCH_OS=FREERTOS -DSCH_ARCH=NANOMIND -DSCH_ST_MODE=FLASH
```

6. Build the project using custom SDK. The Nanomind A3200 SDK uses the WAF
build system so please use the provided build script.
```shell
cd apps/suchai/src/drivers
export LC_ALL=C
sh build.sh
cd -
```

7. (Optional) Program the microcontroller. You require to physically conect
your computer, the AVR Dragon programmer, and the microcontroller. You also
need and USB-Serial cable and a serial terminal application (500000 8n1)
```shell
cd apps/suchai/src/drivers
sh build.sh program
cd -
```

If the step 4 was executed without errors your app is ready to be tested
in the real hardware (Step 5).

**Note 1:** To solve the *add_subdirectory given source "freertos/FreeRTOS" which is not an existing directory*
CMake error and lets the IDE inspect the project correctly its worth to download FreeRTOS source files and custom 
CMakeList.txt from this [link](https://drive.google.com/file/d/1OMQnXF5BLSVh5SHUz_nV01vUTCoKrEhC/view?usp=sharing) and
extract the folder into `suchai-flight-software/src/os/freertos/`. 