/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/cmdAPP.h"

static const char* tag = "cmdAPP";

void cmd_app_init(void)
{
    cmd_add("obc_get_sensors", obc_get_sensors, "", 0);
    cmd_add("obc_update_status", obc_update_status, "", 0);
}

int obc_get_sensors(char *fmt, char *params, int nparams)
{
    float systemp;

#ifdef LINUX
    float millideg;
    FILE *thermal;
    int n;

    LOGD(tag, "Reading obc data in Linux");
    // Reading temp
    thermal = fopen("/sys/class/thermal/thermal_zone0/temp","r");
    n = fscanf(thermal,"%f",&millideg);
    fclose(thermal);
    if(n!= 1)
        return CMD_ERROR;
    // Save temp
    systemp = millideg / 1000;
#elif defined(NANOMIND)
    int rc = 0;
    int16_t tobc1, tobc2 = 0;
    LOGD(tag, "Reading obc data in NANOMIND");
    rc += gs_lm71_read_temp(GS_A3200_SPI_SLAVE_LM71_0, 100, &tobc1); //sensor1
    rc += gs_lm71_read_temp(GS_A3200_SPI_SLAVE_LM71_1, 100, &tobc2); //sensor2
    if(rc != 0)
    {
        LOGE(tag, "Error reading OBC temperatures");
        return CMD_ERROR;
    }
    systemp = ((float)tobc1 + (float)tobc2) / 2.0F;
#else
    systemp = 0;
#endif

    // Save temp
    uint32_t curr_time = (uint32_t)dat_get_time();
    int index_temp = dat_get_system_var(data_map[temp_sensors].sys_index);
    temp_data_t data_temp = {index_temp, curr_time, systemp};
    LOGR(tag, "Time: %u. Temp1: %.1f", curr_time, data_temp.obc_temp_1);
    dat_add_payload_sample(&data_temp, temp_sensors);
    return CMD_OK;
}

int obc_update_status(char *fmt, char *params, int nparams)
{
    float systemp;

#if defined(LINUX)
    float millideg;
    FILE *thermal;
    int n;

    LOGD(tag, "Reading obc data in Linux");
    // Reading temp
    thermal = fopen("/sys/class/thermal/thermal_zone0/temp","r");
    n = fscanf(thermal,"%f",&millideg);
    fclose(thermal);
    if(n!= 1)
        return CMD_ERROR;
#elif defined(NANOMIND)
    int rc = 0;
    int16_t tobc1, tobc2 = 0;
    LOGD(tag, "Reading obc data in NANOMIND");
    rc += gs_lm71_read_temp(GS_A3200_SPI_SLAVE_LM71_0, 100, &tobc1); //sensor1
    rc += gs_lm71_read_temp(GS_A3200_SPI_SLAVE_LM71_1, 100, &tobc2); //sensor2
    if(rc != 0)
    {
        LOGE(tag, "Error reading OBC temperatures");
        return CMD_ERROR;
    }

    // Save temp
    systemp = ((float)tobc1 + (float)tobc2) / 2.0F;
#else
    systemp = 0;
#endif
    /* Set sensors status variables (fix type) */
    value32_t temp_1;
    temp_1.f = systemp;
    dat_set_status_var(dat_obc_temp_1, temp_1);

    LOGR(tag, "Temp1: %.1f", temp_1.f);
    return CMD_OK;
}
